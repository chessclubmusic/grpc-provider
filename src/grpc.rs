//use futures::executor::block_on;
use sdr_grpc_lib::client;
use sdr_grpc_lib::server::{FileRequester, GetFileResults, GrpcServer};
use std::any::type_name;
use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::sync::{Arc, RwLock};
use tokio::runtime::Runtime;
use tokio::sync::{mpsc, oneshot};
use wascc_codec::capabilities::{Dispatcher, NullDispatcher};
//use wascc_codec::serialize;
use wascc_codec::{deserialize, serialize};

use crate::{
    FileRequestMessage, FileRequestResponse, PushFileMessage, RequestToSendMessage,
    OP_DELIVER_REQUESTED_FILE, OP_REQUEST_FILE,
};

const ENV_GRPC_OXIDE_URL: &str = "OXIDE_URL"; // url where server will run to respond to requests
const ENV_GRPC_SDR_URL: &str = "SDR_URL"; // whom to send requests as client
const ENV_GRPC_OXIDE_ID: &str = "OXIDE_ID"; // ID of this node

fn type_of<T>(_: T) -> &'static str {
    type_name::<T>()
}

struct SdrFileRequester {
    request_dispatcher: Arc<RwLock<Box<dyn Dispatcher>>>,
    actor: String,
}

impl SdrFileRequester {
    pub fn new() -> Self {
        SdrFileRequester {
            request_dispatcher: Arc::new(RwLock::new(Box::new(NullDispatcher::new()))),
            actor: String::new(),
        }
    }

    pub fn configure_requester(
        mut self,
        requester: Arc<RwLock<Box<dyn Dispatcher>>>,
        actor: &str,
    ) -> Self {
        self.request_dispatcher = requester.clone();
        //*lock = *(requester.read().unwrap());
        self.actor = actor.to_string();
        self
        //Ok(())
    }
}

//XXX: note: f_name is really "{}/{}" f_containter, f_name
impl FileRequester for SdrFileRequester {
    fn get_file(&self, f_name: &str) -> oneshot::Receiver<GetFileResults> {
        let (sender, receiver) = oneshot::channel::<GetFileResults>();
        let f_req_msg = FileRequestMessage {
            file_name: f_name.to_string(),
        };
        let d = self.request_dispatcher.clone();
        //println!("Dispatcher {}", d);
        let _u8_msg: &[u8] = &serialize(f_req_msg.clone()).unwrap();
        let actor_resp = d.read().unwrap().dispatch(
            &self.actor.clone(),
            OP_REQUEST_FILE,
            &serialize(f_req_msg).unwrap(),
        );
        if actor_resp.is_err() {
            println!("actor_resp is ERROR!!");
            println!("actor_resp: {:?}", actor_resp);
        }
        debug!("actor_resp is not the error");
        match actor_resp {
            Ok(f) => {
                //let str_from_bytes = std::str::from_utf8(&f).unwrap();
                //warn!("f as passed from get_file(): {}", str_from_bytes);
                debug!("about to deserialize message");
                let resp_msg: FileRequestResponse = deserialize(&f.clone()).unwrap();
                let check_string = String::from_utf8(resp_msg.file_contents.clone())
                    .unwrap_or(String::from("FAILED TO CONVERT TO STRING"));
                debug!("Sending {} over gRPC", check_string);

                //TODO:
                // XXX XXX XXX XXX
                // SOLUTION: Open another channel, wait to have results pushed to it!! (??)
                // HACK!!
                let mock_f_name = "/home/alexander/demo_dir/test_file.txt";
                let file_result = fs::read(mock_f_name);
                //let file_result = fs::read(f_name);
                match file_result {
                    Ok(f_bytes) => sender.send(Ok(f_bytes)),
                    Err(e) => sender.send(Err(Box::new(e))),
                }
            }
            Err(e) => sender.send(Err(e)),
        };

        receiver
    }
}

pub(crate) fn push_file(
    _dispatcher: Arc<RwLock<Box<dyn Dispatcher>>>,
    actor: &str,
    clnt: client::Client,
    msg: PushFileMessage,
) -> Result<Vec<u8>, Box<dyn Error + Sync + Send>> {
    debug!("push_file(): attempting to push file {}", msg.file_path);
    let _a = actor.to_string();
    let file_as_bytes = msg.file_contents.clone();
    std::thread::spawn(move || {
        let rt = Runtime::new().unwrap();
        let file_response = clnt.send_bytes_stream(
            256 as i32,
            file_as_bytes,
            msg.request_id,
            msg.destination_id,
            msg.vlan_id,
        );
        let _resolved_future = rt.block_on(file_response);
        warn!("push_file(): received response from client confirming file delivery!");
    });
    Ok(vec![])
}

pub(crate) fn request_file(
    dispatcher: Arc<RwLock<Box<dyn Dispatcher>>>,
    actor: &str,
    clnt: client::Client,
    msg: RequestToSendMessage,
) -> Result<Vec<u8>, Box<dyn Error + Sync + Send>> {
    //) -> Result<Vec<u8>, Box<dyn Error + Sync + Send>> {
    // XXX
    // set up 'SendReceiveClient'
    //warn!("inside request_file()!!");
    let a = actor.to_string();
    std::thread::spawn(move || {
        let rt = Runtime::new().unwrap();
        let file_response = clnt.request_file(&msg.request_id, &msg.source_id, &msg.file_name);
        let resolved_future = rt.block_on(file_response);
        warn!("request_file(): received results from grpc client!");
        match resolved_future {
            Ok(r) => {
                debug!("File response is ok");
                let r_as_contents: Vec<u8> = r;
                let msg_to_actor = FileRequestResponse {
                    file_contents: r_as_contents,
                };
                //warn!("type_of(msg_to_actor): {}", type_of(msg_to_actor.clone()));
                let serialized_msg = &serialize(&msg_to_actor).unwrap();
                let byte_arry: &[u8] = serialized_msg;
                let d = dispatcher.read().unwrap();
                d.dispatch(&a, OP_DELIVER_REQUESTED_FILE, byte_arry)
            }
            Err(e) => {
                error!("GRPC client failed in request_file(): {:?}", e);
                Err("request_file failed".into())
            }
        }
    });
    Ok(vec![])
}

pub(crate) fn initialize_client(
    _dispatcher: Arc<RwLock<Box<dyn Dispatcher>>>,
    _actor: &str,
    values: &HashMap<String, String>,
) -> Result<client::Client, Box<dyn Error + Sync + Send>> {
    // url of *THIS* gRPC server
    let sdr_url = match values.get(ENV_GRPC_SDR_URL) {
        Some(v) => v,
        None => "http://[::1]:50051",
    }
    .to_string();
    if values.get(ENV_GRPC_OXIDE_ID).is_none() {
        return Err("Oxide Node ID not configured".into());
    }
    let oxide_id = values.get(ENV_GRPC_OXIDE_ID).unwrap().to_string();
    info!("initialize_client(), sdr_url: {}", sdr_url);
    let client_config = client::ClientConfig::default().set_uri_from_string(sdr_url);
    let clnt = client::Client::new()
        .set_config(client_config)
        .set_node_id(oxide_id);
    //TODO: idk what to do here??
    Ok(clnt)
}

pub(crate) fn spawn_server(
    dispatcher: Arc<RwLock<Box<dyn Dispatcher>>>,
    actor: &str,
    values: &HashMap<String, String>,
) -> Result<(), Box<dyn Error + Sync + Send>> {
    let service_url = match values.get(ENV_GRPC_OXIDE_URL) {
        Some(v) => v,
        None => "http://[::1]:50052",
    }
    .to_string();
    info!("spawn_server(), service_url: {}", service_url);

    let a = actor.to_string();
    //TODO! add interceptor!!
    std::thread::spawn(move || {
        let d = dispatcher.clone();
        let rt = Runtime::new().unwrap();
        rt.block_on(async {
            let (tx, mut rx) = mpsc::unbounded_channel();
            let mut f_requester = SdrFileRequester::new();
            f_requester = f_requester.configure_requester(d, &a);

            let res = GrpcServer::default()
                .set_service_address(service_url)
                .spawn_locking_with_requester(Box::new(f_requester));

            tokio::spawn(async move {
                if let Err(e) = res.await {
                    eprintln!("spawn_server() error: {:?}", e);
                }

                tx.send(()).unwrap();
            });

            rx.recv().await;
        });
    });

    Ok(())
}
