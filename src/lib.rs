// Copyright 2015-2020 Capital One Services, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

mod grpc;

#[macro_use]
extern crate wascc_codec as codec;

const VERSION: &str = env!("CARGO_PKG_VERSION");
const REVISION: u32 = 2; // Increment for each crates publish

#[macro_use]
extern crate log;

use codec::capabilities::{
    CapabilityDescriptor, CapabilityProvider, Dispatcher, NullDispatcher, OperationDirection,
    OP_GET_CAPABILITY_DESCRIPTOR,
};
use codec::core::{OP_BIND_ACTOR, OP_REMOVE_ACTOR};
use codec::{messaging::OP_DELIVER_MESSAGE, serialize};
//messaging::{RequestMessage, OP_DELIVER_MESSAGE, OP_PERFORM_REQUEST, OP_PUBLISH_MESSAGE},
//use natsclient;
use sdr_grpc_lib::client;
use std::collections::HashMap;
use wascc_codec::core::CapabilityConfiguration;
use wascc_codec::deserialize;

use std::error::Error;
use std::sync::Arc;
use std::sync::RwLock;

#[cfg(not(feature = "static_plugin"))]
capability_provider!(GrpcProvider, GrpcProvider::new);

const CAPABILITY_ID: &str = "wascc:grpcsdr";

pub(crate) const OP_REQUEST_FILE: &str = "RequestFile";
pub(crate) const OP_PUSH_FILE: &str = "PushFile";
pub(crate) const OP_SEND_FILE_REQUEST: &str = "SendFileRequest";
pub(crate) const OP_DELIVER_REQUESTED_FILE: &str = "DeliverRequestedFile";

/// NATS implementation of the `wascc:messaging` specification
pub struct GrpcProvider {
    dispatcher: Arc<RwLock<Box<dyn Dispatcher>>>,
    clients: Arc<RwLock<HashMap<String, client::Client>>>,
}

impl Default for GrpcProvider {
    fn default() -> Self {
        match env_logger::try_init() {
            Ok(_) => {}
            Err(_) => {}
        };

        GrpcProvider {
            dispatcher: Arc::new(RwLock::new(Box::new(NullDispatcher::new()))),
            clients: Arc::new(RwLock::new(HashMap::new())),
        }
    }
}

impl GrpcProvider {
    /// Creates a new NATS provider. This is either invoked manually in static plugin
    /// mode, or invoked by the host during dynamic loading
    pub fn new() -> GrpcProvider {
        Self::default()
    }

    fn service_request(
        &self,
        actor: &str,
        msg: RequestToSendMessage,
    ) -> Result<Vec<u8>, Box<dyn Error + Sync + Send>> {
        let lock = self.clients.read().unwrap();
        let client_ref = lock.get(actor).unwrap();
        let client = (*client_ref).clone();
        let req_res = grpc::request_file(self.dispatcher.clone(), actor.clone(), client, msg);
        req_res
    }

    fn push_file(
        &self,
        actor: &str,
        msg: PushFileMessage,
    ) -> Result<Vec<u8>, Box<dyn Error + Sync + Send>> {
        let lock = self.clients.read().unwrap();
        let client_ref = lock.get(actor).unwrap();
        let client = (*client_ref).clone();
        let req_res = grpc::push_file(self.dispatcher.clone(), actor.clone(), client, msg);
        req_res
    }
    fn configure(
        &self,
        msg: CapabilityConfiguration,
    ) -> Result<Vec<u8>, Box<dyn Error + Sync + Send>> {
        let d = self.dispatcher.clone();
        // XXX
        // TODO: call spawn_server here!!
        let mod_id = msg.module.clone();
        let c = grpc::initialize_client(d, &msg.module, &msg.values)?;

        self.clients.write().unwrap().insert(msg.module, c);
        grpc::spawn_server(self.dispatcher.clone(), &mod_id, &msg.values)?;
        Ok(vec![])
    }

    fn remove_actor(
        &self,
        msg: CapabilityConfiguration,
    ) -> Result<Vec<u8>, Box<dyn Error + Sync + Send>> {
        info!("Removing NATS client for actor {}", msg.module);
        self.clients.write().unwrap().remove(&msg.module);
        Ok(vec![])
    }

    fn get_descriptor(&self) -> Result<Vec<u8>, Box<dyn Error + Sync + Send>> {
        Ok(serialize(
            CapabilityDescriptor::builder()
                .id(CAPABILITY_ID)
                .name("SDR GRPC provider")
                .long_description("A NATS-based implementation of the wascc:messaging contract")
                .version(VERSION)
                .revision(REVISION)
                .with_operation(
                    OP_SEND_FILE_REQUEST,
                    OperationDirection::ToProvider,
                    "Sends a message on a subject expecting a reply on an auto-generated inbox",
                )
                .with_operation(
                    OP_DELIVER_REQUESTED_FILE,
                    OperationDirection::ToActor,
                    "Delivers a file requested to an actor",
                )
                .with_operation(
                    OP_PUSH_FILE,
                    OperationDirection::ToProvider,
                    "call to provider to push file",
                )
                .with_operation(
                    OP_DELIVER_MESSAGE,
                    OperationDirection::ToActor,
                    "Delivers a message from a NATS subscription to an actor",
                )
                .with_operation(
                    OP_REQUEST_FILE,
                    OperationDirection::ToActor,
                    "Requests file from actor to be streamed",
                )
                .build(),
        )?)
    }
}

impl CapabilityProvider for GrpcProvider {
    /// Receives a dispatcher from the host runtime
    fn configure_dispatch(
        &self,
        dispatcher: Box<dyn Dispatcher>,
    ) -> Result<(), Box<dyn Error + Sync + Send>> {
        //) -> Result<(), Box<dyn Error + Sync + Send>> {
        warn!("Dispatcher received.");
        let mut lock = self.dispatcher.write().unwrap();
        *lock = dispatcher;

        Ok(())
    }

    /// Handles an invocation received from the host runtime
    fn handle_call(
        &self,
        actor: &str,
        op: &str,
        msg: &[u8],
    ) -> Result<Vec<u8>, Box<dyn Error + Sync + Send>> {
        warn!("Received host call from {}, operation - {}", actor, op);

        match op {
            OP_SEND_FILE_REQUEST => self.service_request(actor, deserialize(msg)?),
            OP_PUSH_FILE => self.push_file(actor, deserialize(msg)?),
            OP_GET_CAPABILITY_DESCRIPTOR if actor == "system" => self.get_descriptor(),
            OP_BIND_ACTOR if actor == "system" => self.configure(deserialize(msg)?),
            OP_REMOVE_ACTOR if actor == "system" => self.remove_actor(deserialize(msg)?),
            _ => Err("bad dispatch".into()),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FileRequestMessage {
    pub file_name: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FileRequestResponse {
    pub file_contents: Vec<u8>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RequestToSendMessage {
    pub file_name: String,
    pub request_id: String,
    pub source_id: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PushFileMessage {
    pub file_path: String,
    pub request_id: String,
    pub destination_id: String,
    pub vlan_id: String,
    pub file_contents: Vec<u8>,
}
